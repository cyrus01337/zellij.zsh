# Zsh plugin for Zellij

## About

A small zsh plugin for autostarting [Zellij](https://zellij.dev/)
as your terminal multiplexer.

Before your shell has started,
checks if the `zellij` command is available and no zellij session is running,
then starts a new session and attaches to it.

## Installation

### [zplug](https://github.com/zplug/zplug)

Add to your `.zshrc`:

```zsh
zplug "tranzystorek-io/zellij-zsh"
```

Then, run `zplug install` and restart your terminal.

### [sheldon](https://github.com/rossmacarthur/sheldon)

Run:

```console
sheldon add --git https://codeberg.org/tranzystorekk/zellij.zsh zellij-zsh
```

### [ohmyzsh](https://github.com/ohmyzsh/ohmyzsh)

Run:

```zsh
git clone https://codeberg.org/tranzystorekk/zellij.zsh.git $ZSH/custom/plugins/zellij
```

Then, add to `plugins` and restart your terminal:

```zsh
plugins=(
    zellij
    ...
)
```

## Configuration

### `ZELLIJ_ZSH_DISABLE_PLUGIN`

Disables this plugin:

```shell
# in your .zshenv file
export ZELLIJ_ZSH_DISABLE_PLUGIN=1
```

### `ZELLIJ_ZSH_DISABLE_VSCODE`

Disables this plugin if running in a Visual Studio Code Terminal:

```shell
# in your .zshenv file
export ZELLIJ_ZSH_DISABLE_VSCODE=1
```

### `ZELLIJ_ZSH_SESSION_NAME`

Sets fixed session name that `zellij` will be attached to:

```shell
# in your .zshenv file
export ZELLIJ_ZSH_SESSION_NAME=main
```

Note: this means that opening another terminal will share a single session.

### `ZELLIJ_ZSH_AUTO_ATTACH`

Attach `zellij` to an existing session automatically:

```shell
# in your .zshenv file
export ZELLIJ_ZSH_AUTO_ATTACH=1
```

This works similar to `ZELLIJ_ZSH_SESSION_NAME` except random session name is used.
Also, `ZELLIJ_ZSH_SESSION_NAME` must be empty/unset.

### `ZELLIJ_ZSH_DEBUG_MODE`

Enables additional debug logs in `zellij`:

```shell
# in your .zshenv file
export ZELLIJ_ZSH_DEBUG_MODE=1
```
